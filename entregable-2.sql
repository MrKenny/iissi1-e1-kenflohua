SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS Degrees; #se encarga de que se borre la tabla si ya existía
DROP TABLE IF EXISTS Subjects;
DROP TABLE IF EXISTS Groups;
DROP TABLE IF EXISTS Students;
DROP TABLE IF EXISTS GroupsStudents;
DROP TABLE IF EXISTS Grades;
DROP TABLE IF EXISTS Teachers;
DROP TABLE IF EXISTS Departments;
DROP TABLE IF EXISTS Tutorships;
DROP TABLE IF EXISTS Dates;
DROP TABLE IF EXISTS Classrooms;
DROP TABLE IF EXISTS Offices;
SET FOREIGN_KEY_CHECKS=1;

#despacho
CREATE TABLE Offices(
	officeId INT NOT NULL AUTO_INCREMENT,
	plant VARCHAR(30) NOT NULL UNIQUE,
	capacity INT NOT NULL UNIQUE,
	PRIMARY KEY (officeId),
	CONSTRAINT negativeCapacity CHECK (capacity>0)
);

INSERT INTO Offices(plant, capacity) VALUES ('G1.40', 4);
INSERT INTO Offices(plant, capacity) VALUES ('G0.67', 5);
INSERT INTO Offices(plant, capacity) VALUES ('G1.67', 7);

#Departamentos
CREATE TABLE Departments(
	departmentId INT NOT NULL AUTO_INCREMENT,
	name_dep VARCHAR(100) NOT NULL,
	PRIMARY KEY (departmentId)
);	

INSERT INTO Departments(name_dep) VALUES ("Tecnología Electrónica");
INSERT INTO Departments(name_dep) VALUES ("Física Aplicada");
INSERT INTO Departments(name_dep) VALUES ("Matemática Aplicada I");

#Profesor
CREATE TABLE Teachers(
	teacherId INT NOT NULL AUTO_INCREMENT,
	dni CHAR(9) NOT NULL UNIQUE,
	firstName VARCHAR(100) NOT NULL,
	surname VARCHAR(100) NOT NULL,
	birthDate DATE NOT NULL,
	email VARCHAR(250) NOT NULL UNIQUE,
	category VARCHAR(30) NOT NULL,
	creditsGiven INT NOT NULL,
	officeId INT NOT NULL,
	departmentId INT NOT NULL,
	PRIMARY KEY (teacherId),
	FOREIGN KEY (officeId) REFERENCES Offices (officeId),
	FOREIGN KEY (departmentId) REFERENCES Departments (departmentId),
	CONSTRAINT invalidTeacherCategory CHECK (category IN ('Catedratico', 'Titular_Universidad', 'Profesor_Contratado_Doctor', 'Profesor_Ayudante_Doctor')),
	CONSTRAINT invalidCreditsperCourse CHECK (creditsGiven <= 24)
);

INSERT INTO Teachers(dni, firstName, surname, birthDate, email, category, creditsGiven, officeId, DepartmentId) VALUES ('54239628G', 'Vicente', 'Losada Torres', '1970-6-11', 'losada@us.es','Titular_Universidad', 9,1,2);
INSERT INTO Teachers(dni, firstName, surname, birthDate, email, category, creditsGiven, officeId, DepartmentId) VALUES ('54219627Y', 'Juan Carlos', 'Dana Jiménez', '1975-1-12', 'dana@us.es','Titular_Universidad',  9,2,3);
INSERT INTO Teachers(dni, firstName, surname, birthDate, email, category, creditsGiven, officeId, DepartmentId) VALUES ('54213946T', 'Maria del Pilar', 'Parra Fernandez', '1980-3-15', 'pparra@us.es','Titular_Universidad', 9,3,1);

# GRADO
CREATE TABLE Degrees(
	degreeId INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(60) NOT NULL UNIQUE, # UNIQUE -> UN ATRIBUTO NO PUEDE TOMAR VALORES REPETIDOS
	years INT DEFAULT(4) NOT NULL, # UN ATRIBUTO NO PUEDE SER NULO
	departmentId INT NOT NULL,
	PRIMARY KEY (degreeId),
	FOREIGN KEY (departmentId) REFERENCES Departments (departmentId),
	CONSTRAINT invalidDegreeYear CHECK (years >=3 AND years <=5)
);

INSERT INTO Degrees (name, years, departmentId) VALUE ('Tecnologías Informaticas', 4, 1);
INSERT INTO Degrees (name, years, departmentId) VALUE ('Ingenería de Software', 4, 2);
INSERT INTO Degrees (name, years, departmentId ) VALUE ('Ingeniería de Computadores', 4, 3);

#Alumnos
CREATE TABLE Students(
	studentId INT NOT NULL AUTO_INCREMENT,
	dni CHAR(9) NOT NULL UNIQUE,
	accessMethod VARCHAR(30) NOT NULL,
	firstName VARCHAR(100) NOT NULL,
	surname VARCHAR(100) NOT NULL,
	birthDate DATE NOT NULL,
	email VARCHAR(250) NOT NULL UNIQUE,
	credits INT NOT NULL,
	degreeId INT NOT NULL,
	PRIMARY KEY (studentId),
	FOREIGN KEY (degreeId) REFERENCES Degrees (degreeId),
	CONSTRAINT invalidStudentAccessMethod CHECK (accessMethod IN ('Selectividad', 'Ciclo', 'Mayor', 'Titulado Extranjero')),
	CONSTRAINT invalidStudentCredits CHECK (credits >=30 OR credits<90)
);

INSERT INTO Students (dni,accessMethod, firstName, surname, birthDate, email, credits,degreeId) VALUE ('54219624G','Selectividad', 'Kenny', 'Flores Huaman', '2000-6-1', 'kennyjesus@pm.me', 66,2);
INSERT INTO Students (dni,accessMethod,  firstName, surname, birthDate, email, credits,degreeId) VALUE ('54219539X', 'Ciclo','Eduardo', 'Gonzalez Perez', '1996-12-1', 'eduardogp@protonmail.ch', 60,2);
INSERT INTO Students (dni,accessMethod, firstName, surname, birthDate, email, credits,degreeId) VALUE ( '54219548Y','Ciclo', 'Rebeca', 'Sarai', '1997-11-1', 'rsarai@mail.ru', 66,3);



#TUTORIA
CREATE TABLE Tutorships(
	tutorshipId INT NOT NULL AUTO_INCREMENT,
	dayofweek DATE NOT NULL,
	startTime TIME NOT NULL,
	endTime TIME NOT NULL,
	teacherId INT NULL,
	PRIMARY KEY (tutorshipId),
	FOREIGN KEY (teacherId) REFERENCES Teachers (teacherId)
);

INSERT INTO Tutorships (dayofweek, startTime, endTime, teacherId) VALUE ('2019-1-12', '09:00:00', '09:15:00', 1);
INSERT INTO Tutorships (dayofweek, startTime, endTime, teacherId) VALUE ('2019-1-12', '10:00:00', '10:10:00', 2);
INSERT INTO Tutorships (dayofweek, startTime, endTime, teacherId) VALUE ('2019-1-12', '10:30:00', '11:00:00', 3);

#CITA
CREATE TABLE Dates(
	dateId INT NOT NULL AUTO_INCREMENT,
	date_eng DATE NOT NULL,
	startTime TIME NOT NULL,
	studentId INT NOT NULL,
	tutorshipId INT NOT NULL,
	PRIMARY KEY (dateId),
	FOREIGN KEY (studentId) REFERENCES Students (studentId),
	FOREIGN KEY (tutorshipId) REFERENCES Tutorships (tutorshipId)
);

INSERT INTO Dates (date_eng, startTime, studentId, tutorshipId) VALUE ('2019-1-12','09:00:00',1,1);
INSERT INTO Dates (date_eng, startTime, studentId, tutorshipId) VALUE ('2019-1-12','10:00:00',2,2);
INSERT INTO Dates (date_eng, startTime, studentId, tutorshipId) VALUE ('2019-1-12','10:30:00',3,3);

#AULA
CREATE TABLE Classrooms(

	classroomId INT NOT NULL AUTO_INCREMENT,
	NAME VARCHAR(30) NOT NULL UNIQUE,
	floor VARCHAR(30) NOT NULL,
	capacity INT NOT NULL,
	megaphony BOOLEAN NOT NULL,
	projector BOOLEAN NOT NULL,
	PRIMARY KEY (classroomId),
	CONSTRAINT negativeCapacity CHECK (capacity>0)
);

INSERT INTO Classrooms(NAME, floor, capacity, megaphony, projector) VALUES ('A2.17', 2, 90, TRUE, FALSE);
INSERT INTO Classrooms(NAME, floor, capacity, megaphony, projector) VALUES ('G1.33', 1, 20, FALSE, FALSE);
INSERT INTO Classrooms(NAME, floor, capacity, megaphony, projector) VALUES ('A3.17', 3, 90, FALSE, TRUE);

#ASIGNATURA
CREATE TABLE Subjects(
	subjectId INT AUTO_INCREMENT NOT NULL AUTO_INCREMENT,
	name VARCHAR(100) NOT NULL UNIQUE,
	acronym VARCHAR(8) NOT NULL UNIQUE,
	credits INT NOT NULL,
	course INT NOT NULL,
	type VARCHAR(20) NOT NULL,
	degreeId INT NOT NULL,
	PRIMARY KEY (subjectId),
	FOREIGN KEY (degreeId) REFERENCES Degrees (degreeId),
	CONSTRAINT negativeSubjectCredits CHECK (credits > 0),
	CONSTRAINT invalidSubjectCourse CHECK (course > 0 AND course < 6),
	CONSTRAINT invalidSubjectType CHECK (type IN ('Formacion Basica', 'Optativa', 'Obligatoria'))
);

INSERT INTO Subjects (name, acronym, credits, course, type, degreeId) VALUES ('Circuitos Digitales y Electrónicos', 'CED', 6, 1, 'Formacion Basica',1);
INSERT INTO Subjects (name, acronym, credits, course, type, degreeId) VALUES ('Cálculo Infinitesimal y numérico', 'CIN', 6, 1, 'Formacion Basica', 3);
INSERT INTO Subjects (name, acronym, credits, course, type, degreeId) VALUES ('Fundamentos Físicos de la Informática', 'FFI', 6, 1, 'Formacion Basica', 2);


#GRUPO
CREATE TABLE Groups(
	groupId INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(30) NOT NULL,
	activity VARCHAR(20) NOT NULL,
	year INT NOT NULL,
	subjectId INT NOT NULL,
	classroomId INT NOT NULL,
	PRIMARY KEY (groupId),
	FOREIGN KEY (subjectId) REFERENCES Subjects (subjectId),
	FOREIGN KEY (classroomId) REFERENCES Classrooms (classroomId),
	UNIQUE (name, year, subjectId),
	CONSTRAINT negativeGroupYear CHECK (year > 0),
	CONSTRAINT invalidGroupActivity CHECK (activity IN ('Teoria', 'Laboratorio'))
	
);

INSERT INTO Groups (name, activity, year, subjectId, classroomId) VALUES ('TI-2', 'Teoria', 2019, 1,1);
INSERT INTO Groups (name, activity, year, subjectId, classroomId) VALUES ('TI-3', 'Teoria', 2019, 1,3);
INSERT INTO Groups (name, activity, year, subjectId, classroomId) VALUES ('TI-1', 'Teoria', 2019, 1,2);

CREATE TABLE GroupsStudents(
	groupStudentId INT NOT NULL AUTO_INCREMENT,
	groupId INT NOT NULL,
	studentId INT NOT NULL,
	PRIMARY KEY (groupStudentId),
	FOREIGN KEY (groupId) REFERENCES Groups (groupId),
	FOREIGN KEY (studentId) REFERENCES Students (studentId),
	UNIQUE (groupId, studentId)
);

#NOTA
CREATE TABLE Grades(
	gradeId INT NOT NULL AUTO_INCREMENT,
	qualification DECIMAL(4,2) NOT NULL,
	gradeCall INT NOT NULL,
	withHonours BOOLEAN NOT NULL,
	studentId INT NOT NULL,
	groupId INT NOT NULL,
	PRIMARY KEY (gradeId),
	FOREIGN KEY (studentId) REFERENCES Students (studentId),
	FOREIGN KEY (groupId) REFERENCES Groups (groupId),
	CONSTRAINT invalidGradeValue CHECK (qualification >= 0 AND qualification <=10),
	CONSTRAINT invalidGradeCall CHECK (gradeCall >=1 AND gradeCall <=3),
	CONSTRAINT duplicatedCallGrade UNIQUE (gradeCall, studentId, groupId)
); 

INSERT INTO Grades (qualification, gradeCall, withHonours, studentId, groupId) VALUE (07.00, 2, FALSE, 1,1);
INSERT INTO Grades (qualification, gradeCall, withHonours, studentId, groupId) VALUE (05.75, 2, FALSE, 2,2);
INSERT INTO Grades (qualification, gradeCall, withHonours, studentId, groupId) VALUE (09.50, 1, TRUE, 3,3);