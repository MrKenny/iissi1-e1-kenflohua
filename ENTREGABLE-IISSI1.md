# EJERCICIO IISSI ENTREGABLE 1:

## 1. Identificar posibles entidades y atributos


|Entidades   | Atributos  |
|---|---|
| Profesor | DNI, nombre, apellidos, fecha_nacimiento, email, categoria, espacio, credito, departamento|
|Tutoria| dia_semana, hora_comienzo, hora_fin, **profesor**  |
| Cita  | fecha, hora, alumno|
|  Espacio | nombre_espacio, planta, capacidad|
| Asignatura  |nombre, acrónimo, créditos, curso, tipo, grupo, profesor,**grado**|
| Departamento   | nombre_departamento, asignatura, profesor  |
| Alumno | DNI, nombre, apellidos, fecha_ nacimiento, email, metodo_acceso,credito, nota|
| Nota | asignatura, grupo, valor,  n_convocatorias, matricula_honor |
|Grado | nombre, n_años, asignatura  |
|Grupo | nombre, actividad,año, alumno, asignatura|

<!--Categoria ->  Catedrático, Titular de
Universidad, Profesor Contratado Doctor, Profesor Ayudante Doctor-->
<!--tipo (Obligatoria,
Optativa, Formación Básica)-->
<!--Método acceso -> Selectividad, Ciclo, Mayor, Titulado Extranjero-->
<!--tipo_espacio -> aulas o despachos-->

## 2. Identificar posibles asociaciones.
|Entidades   | Atributos  |
|---|---|
| Profesor | DNI, nombre, apellidos, fecha_nacimiento, email, categoria, **espacio**, credito, **departamento**|
|Tutoria| dia_semana, hora_comienzo, hora_fin |
| Cita  | fecha, hora, **alumno**|
|  Espacio | nombre_espacio, planta, capacidad|
| Asignatura  |nombre, acrónimo, créditos, curso, tipo, **grupo**, **profesor**,**grado**| //revisar
| Departamento   | nombre_departamento, **asignatura**, **profesor**|
| Alumno | DNI, nombre, apellidos, fecha_ nacimiento, email, metodo_acceso,credito, **nota**|
| Nota | **asignatura**, **grupo**, valor,  n_convocatorias, matricula_honor |
|Grado | nombre, n_años, **asignatura** |
|Grupo | nombre, actividad,año, **alumno**, **asignatura**|

// Pendientes de saber si son entidad son AULA y DESPACHO
