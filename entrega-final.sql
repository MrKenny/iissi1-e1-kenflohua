-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.8-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para grados
CREATE DATABASE IF NOT EXISTS `grados` /*!40100 DEFAULT CHARACTER SET latin1 COLLATE latin1_spanish_ci */;
USE `grados`;

-- Volcando estructura para tabla grados.classrooms
DROP TABLE IF EXISTS `classrooms`;
CREATE TABLE IF NOT EXISTS `classrooms` (
  `classroomId` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `floor` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `capacity` int(11) NOT NULL,
  `megaphony` tinyint(1) NOT NULL,
  `projector` tinyint(1) NOT NULL,
  PRIMARY KEY (`classroomId`),
  UNIQUE KEY `NAME` (`NAME`),
  CONSTRAINT `negativeCapacity` CHECK (`capacity` > 0)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla grados.classrooms: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `classrooms` DISABLE KEYS */;
REPLACE INTO `classrooms` (`classroomId`, `NAME`, `floor`, `capacity`, `megaphony`, `projector`) VALUES
	(1, 'A2.17', '2', 90, 1, 0),
	(2, 'G1.33', '1', 20, 0, 0),
	(3, 'A3.17', '3', 90, 0, 1);
/*!40000 ALTER TABLE `classrooms` ENABLE KEYS */;

-- Volcando estructura para tabla grados.dates
DROP TABLE IF EXISTS `dates`;
CREATE TABLE IF NOT EXISTS `dates` (
  `dateId` int(11) NOT NULL AUTO_INCREMENT,
  `date_eng` date NOT NULL,
  `startTime` time NOT NULL,
  `studentId` int(11) NOT NULL,
  `tutorshipId` int(11) NOT NULL,
  PRIMARY KEY (`dateId`),
  KEY `studentId` (`studentId`),
  KEY `tutorshipId` (`tutorshipId`),
  CONSTRAINT `dates_ibfk_1` FOREIGN KEY (`studentId`) REFERENCES `students` (`studentId`),
  CONSTRAINT `dates_ibfk_2` FOREIGN KEY (`tutorshipId`) REFERENCES `tutorships` (`tutorshipId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla grados.dates: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `dates` DISABLE KEYS */;
REPLACE INTO `dates` (`dateId`, `date_eng`, `startTime`, `studentId`, `tutorshipId`) VALUES
	(1, '2019-01-12', '09:00:00', 1, 1),
	(2, '2019-01-12', '10:00:00', 2, 2),
	(3, '2019-01-12', '10:30:00', 3, 3);
/*!40000 ALTER TABLE `dates` ENABLE KEYS */;

-- Volcando estructura para tabla grados.degrees
DROP TABLE IF EXISTS `degrees`;
CREATE TABLE IF NOT EXISTS `degrees` (
  `degreeId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE latin1_spanish_ci NOT NULL,
  `years` int(11) NOT NULL DEFAULT 4,
  `departmentId` int(11) NOT NULL,
  PRIMARY KEY (`degreeId`),
  UNIQUE KEY `name` (`name`),
  KEY `departmentId` (`departmentId`),
  CONSTRAINT `degrees_ibfk_1` FOREIGN KEY (`departmentId`) REFERENCES `departments` (`departmentId`),
  CONSTRAINT `invalidDegreeYear` CHECK (`years` >= 3 and `years` <= 5)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla grados.degrees: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `degrees` DISABLE KEYS */;
REPLACE INTO `degrees` (`degreeId`, `name`, `years`, `departmentId`) VALUES
	(1, 'Tecnologías Informaticas', 4, 1),
	(2, 'Ingenería de Software', 4, 2),
	(3, 'Ingeniería de Computadores', 4, 3);
/*!40000 ALTER TABLE `degrees` ENABLE KEYS */;

-- Volcando estructura para tabla grados.departments
DROP TABLE IF EXISTS `departments`;
CREATE TABLE IF NOT EXISTS `departments` (
  `departmentId` int(11) NOT NULL AUTO_INCREMENT,
  `name_dep` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`departmentId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla grados.departments: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
REPLACE INTO `departments` (`departmentId`, `name_dep`) VALUES
	(1, 'Tecnología Electrónica'),
	(2, 'Física Aplicada'),
	(3, 'Matemática Aplicada I');
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;

-- Volcando estructura para tabla grados.grades
DROP TABLE IF EXISTS `grades`;
CREATE TABLE IF NOT EXISTS `grades` (
  `gradeId` int(11) NOT NULL AUTO_INCREMENT,
  `qualification` decimal(4,2) NOT NULL,
  `gradeCall` int(11) NOT NULL,
  `withHonours` tinyint(1) NOT NULL,
  `studentId` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  PRIMARY KEY (`gradeId`),
  UNIQUE KEY `duplicatedCallGrade` (`gradeCall`,`studentId`,`groupId`),
  KEY `studentId` (`studentId`),
  KEY `groupId` (`groupId`),
  CONSTRAINT `grades_ibfk_1` FOREIGN KEY (`studentId`) REFERENCES `students` (`studentId`),
  CONSTRAINT `grades_ibfk_2` FOREIGN KEY (`groupId`) REFERENCES `groups` (`groupId`),
  CONSTRAINT `invalidGradeValue` CHECK (`qualification` >= 0 and `qualification` <= 10),
  CONSTRAINT `invalidGradeCall` CHECK (`gradeCall` >= 1 and `gradeCall` <= 3)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla grados.grades: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `grades` DISABLE KEYS */;
REPLACE INTO `grades` (`gradeId`, `qualification`, `gradeCall`, `withHonours`, `studentId`, `groupId`) VALUES
	(1, 7.00, 2, 0, 1, 1),
	(2, 5.75, 2, 0, 2, 2),
	(3, 9.50, 1, 1, 3, 3);
/*!40000 ALTER TABLE `grades` ENABLE KEYS */;

-- Volcando estructura para tabla grados.groups
DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
  `groupId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `activity` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `year` int(11) NOT NULL,
  `subjectId` int(11) NOT NULL,
  `classroomId` int(11) NOT NULL,
  PRIMARY KEY (`groupId`),
  UNIQUE KEY `name` (`name`,`year`,`subjectId`),
  KEY `subjectId` (`subjectId`),
  KEY `classroomId` (`classroomId`),
  CONSTRAINT `groups_ibfk_1` FOREIGN KEY (`subjectId`) REFERENCES `subjects` (`subjectId`),
  CONSTRAINT `groups_ibfk_2` FOREIGN KEY (`classroomId`) REFERENCES `classrooms` (`classroomId`),
  CONSTRAINT `negativeGroupYear` CHECK (`year` > 0),
  CONSTRAINT `invalidGroupActivity` CHECK (`activity` in ('Teoria','Laboratorio'))
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla grados.groups: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
REPLACE INTO `groups` (`groupId`, `name`, `activity`, `year`, `subjectId`, `classroomId`) VALUES
	(1, 'TI-2', 'Teoria', 2019, 1, 1),
	(2, 'TI-3', 'Teoria', 2019, 1, 3),
	(3, 'TI-1', 'Teoria', 2019, 1, 2);
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;

-- Volcando estructura para tabla grados.groupsstudents
DROP TABLE IF EXISTS `groupsstudents`;
CREATE TABLE IF NOT EXISTS `groupsstudents` (
  `groupStudentId` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `studentId` int(11) NOT NULL,
  PRIMARY KEY (`groupStudentId`),
  UNIQUE KEY `groupId` (`groupId`,`studentId`),
  KEY `studentId` (`studentId`),
  CONSTRAINT `groupsstudents_ibfk_1` FOREIGN KEY (`groupId`) REFERENCES `groups` (`groupId`),
  CONSTRAINT `groupsstudents_ibfk_2` FOREIGN KEY (`studentId`) REFERENCES `students` (`studentId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla grados.groupsstudents: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `groupsstudents` DISABLE KEYS */;
/*!40000 ALTER TABLE `groupsstudents` ENABLE KEYS */;

-- Volcando estructura para tabla grados.offices
DROP TABLE IF EXISTS `offices`;
CREATE TABLE IF NOT EXISTS `offices` (
  `officeId` int(11) NOT NULL AUTO_INCREMENT,
  `plant` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `capacity` int(11) NOT NULL,
  PRIMARY KEY (`officeId`),
  UNIQUE KEY `plant` (`plant`),
  UNIQUE KEY `capacity` (`capacity`),
  CONSTRAINT `negativeCapacity` CHECK (`capacity` > 0)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla grados.offices: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `offices` DISABLE KEYS */;
REPLACE INTO `offices` (`officeId`, `plant`, `capacity`) VALUES
	(1, 'G1.40', 4),
	(2, 'G0.67', 5),
	(3, 'G1.67', 7);
/*!40000 ALTER TABLE `offices` ENABLE KEYS */;

-- Volcando estructura para tabla grados.students
DROP TABLE IF EXISTS `students`;
CREATE TABLE IF NOT EXISTS `students` (
  `studentId` int(11) NOT NULL AUTO_INCREMENT,
  `dni` char(9) COLLATE latin1_spanish_ci NOT NULL,
  `accessMethod` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `firstName` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `surname` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `birthDate` date NOT NULL,
  `email` varchar(250) COLLATE latin1_spanish_ci NOT NULL,
  `credits` int(11) NOT NULL,
  `degreeId` int(11) NOT NULL,
  PRIMARY KEY (`studentId`),
  UNIQUE KEY `dni` (`dni`),
  UNIQUE KEY `email` (`email`),
  KEY `degreeId` (`degreeId`),
  CONSTRAINT `students_ibfk_1` FOREIGN KEY (`degreeId`) REFERENCES `degrees` (`degreeId`),
  CONSTRAINT `invalidStudentAccessMethod` CHECK (`accessMethod` in ('Selectividad','Ciclo','Mayor','Titulado Extranjero')),
  CONSTRAINT `invalidStudentCredits` CHECK (`credits` >= 30 or `credits` < 90)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla grados.students: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
REPLACE INTO `students` (`studentId`, `dni`, `accessMethod`, `firstName`, `surname`, `birthDate`, `email`, `credits`, `degreeId`) VALUES
	(1, '54219624G', 'Selectividad', 'Kenny', 'Flores Huaman', '2000-06-01', 'kennyjesus@pm.me', 66, 2),
	(2, '54219539X', 'Ciclo', 'Eduardo', 'Gonzalez Perez', '1996-12-01', 'eduardogp@protonmail.ch', 60, 2),
	(3, '54219548Y', 'Ciclo', 'Rebeca', 'Sarai', '1997-11-01', 'rsarai@mail.ru', 66, 3);
/*!40000 ALTER TABLE `students` ENABLE KEYS */;

-- Volcando estructura para tabla grados.subjects
DROP TABLE IF EXISTS `subjects`;
CREATE TABLE IF NOT EXISTS `subjects` (
  `subjectId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `acronym` varchar(8) COLLATE latin1_spanish_ci NOT NULL,
  `credits` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  `type` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `degreeId` int(11) NOT NULL,
  PRIMARY KEY (`subjectId`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `acronym` (`acronym`),
  KEY `degreeId` (`degreeId`),
  CONSTRAINT `subjects_ibfk_1` FOREIGN KEY (`degreeId`) REFERENCES `degrees` (`degreeId`),
  CONSTRAINT `negativeSubjectCredits` CHECK (`credits` > 0),
  CONSTRAINT `invalidSubjectCourse` CHECK (`course` > 0 and `course` < 6),
  CONSTRAINT `invalidSubjectType` CHECK (`type` in ('Formacion Basica','Optativa','Obligatoria'))
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla grados.subjects: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `subjects` DISABLE KEYS */;
REPLACE INTO `subjects` (`subjectId`, `name`, `acronym`, `credits`, `course`, `type`, `degreeId`) VALUES
	(1, 'Circuitos Digitales y Electrónicos', 'CED', 6, 1, 'Formacion Basica', 1),
	(2, 'Cálculo Infinitesimal y numérico', 'CIN', 6, 1, 'Formacion Basica', 3),
	(3, 'Fundamentos Físicos de la Informática', 'FFI', 6, 1, 'Formacion Basica', 2);
/*!40000 ALTER TABLE `subjects` ENABLE KEYS */;

-- Volcando estructura para tabla grados.teachers
DROP TABLE IF EXISTS `teachers`;
CREATE TABLE IF NOT EXISTS `teachers` (
  `teacherId` int(11) NOT NULL AUTO_INCREMENT,
  `dni` char(9) COLLATE latin1_spanish_ci NOT NULL,
  `firstName` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `surname` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `birthDate` date NOT NULL,
  `email` varchar(250) COLLATE latin1_spanish_ci NOT NULL,
  `category` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `creditsGiven` int(11) NOT NULL,
  `officeId` int(11) NOT NULL,
  `departmentId` int(11) NOT NULL,
  PRIMARY KEY (`teacherId`),
  UNIQUE KEY `dni` (`dni`),
  UNIQUE KEY `email` (`email`),
  KEY `officeId` (`officeId`),
  KEY `departmentId` (`departmentId`),
  CONSTRAINT `teachers_ibfk_1` FOREIGN KEY (`officeId`) REFERENCES `offices` (`officeId`),
  CONSTRAINT `teachers_ibfk_2` FOREIGN KEY (`departmentId`) REFERENCES `departments` (`departmentId`),
  CONSTRAINT `invalidTeacherCategory` CHECK (`category` in ('Catedratico','Titular_Universidad','Profesor_Contratado_Doctor','Profesor_Ayudante_Doctor')),
  CONSTRAINT `invalidCreditsperCourse` CHECK (`creditsGiven` <= 24)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla grados.teachers: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `teachers` DISABLE KEYS */;
REPLACE INTO `teachers` (`teacherId`, `dni`, `firstName`, `surname`, `birthDate`, `email`, `category`, `creditsGiven`, `officeId`, `departmentId`) VALUES
	(1, '54239628G', 'Vicente', 'Losada Torres', '1970-06-11', 'losada@us.es', 'Titular_Universidad', 9, 1, 2),
	(2, '54219627Y', 'Juan Carlos', 'Dana Jiménez', '1975-01-12', 'dana@us.es', 'Titular_Universidad', 9, 2, 3),
	(3, '54213946T', 'Maria del Pilar', 'Parra Fernandez', '1980-03-15', 'pparra@us.es', 'Titular_Universidad', 9, 3, 1);
/*!40000 ALTER TABLE `teachers` ENABLE KEYS */;

-- Volcando estructura para tabla grados.tutorships
DROP TABLE IF EXISTS `tutorships`;
CREATE TABLE IF NOT EXISTS `tutorships` (
  `tutorshipId` int(11) NOT NULL AUTO_INCREMENT,
  `dayofweek` date NOT NULL,
  `startTime` time NOT NULL,
  `endTime` time NOT NULL,
  `teacherId` int(11) DEFAULT NULL,
  PRIMARY KEY (`tutorshipId`),
  KEY `teacherId` (`teacherId`),
  CONSTRAINT `tutorships_ibfk_1` FOREIGN KEY (`teacherId`) REFERENCES `teachers` (`teacherId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla grados.tutorships: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `tutorships` DISABLE KEYS */;
REPLACE INTO `tutorships` (`tutorshipId`, `dayofweek`, `startTime`, `endTime`, `teacherId`) VALUES
	(1, '2019-01-12', '09:00:00', '09:15:00', 1),
	(2, '2019-01-12', '10:00:00', '10:10:00', 2),
	(3, '2019-01-12', '10:30:00', '11:00:00', 3);
/*!40000 ALTER TABLE `tutorships` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
