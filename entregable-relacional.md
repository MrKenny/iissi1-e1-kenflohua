## Modelo Relacional de la clase Alumno 
# Clase Grado
Degrees(!degreeId, #name, duration)
# Clase Asignatura
Subjects(!subjectId, @gradeId, #name, #acronym, credits, year, type)
# Clase Grupo
Groups(!groupId, @subjectId, name, activity, academicYear)
# Clase Alumno
Students(!studentId, accessMethod, #dni, firstName, surname, birthDate, #email)
# Clase Nota
Grades(!gradeId, @studentId, @groupId, value, gradeCall, withHonours)
# Clase Profesor
Teachers(!teacherId, #dni, firstName, surname, birthDate, #email, category,room)
# Clase Departamento
Departments(!departmentId, @teacherId, #name)
# Clase Tutoria
Tutorships(!tutorshipId, @teacherId, dayofWeek, startTime, endTime)

# Clase Cita
Dates (!date, @tutorshipId, #date, startTime, student)

# Clase 